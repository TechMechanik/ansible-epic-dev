# ansible-epic-dev

Epic Arch Linux setup with Ansible.

## Requirements

- Arch Linux installed
- Ansible installed
- Text editor of your choice (i.e. `neovim`)
- A regular user with sudo access

---

## Breaking Changes

The `@volar/vue-language-server` package has been replaced by `@vue/language-server`. Please run the following to delete the deprecated packages, if needed.

```bash
npm --prefix ~/.local/epic_npm/ uninstall @volar/vue-language-server
```

## Roles

The following role groups are provided:

- base: base configuration, fish setup, etc. **Note**: this role is automatically applied to all machines and may be skipped in the `inventory` file.
- gamer: installs DOSBox, fluidsynth for midi, etc.
- gui: installs common KDE software like yakuake, flatpak, keepassxc, etc.
- vim (vimmer role): installs neovim, language servers, programming utilities, pypi packages, lazy plugin manager, and configures nvim for LSP, telescope, keyboard shortcuts, etc.

## Installation

First, clone the respository in a location of your choice. My preference is under `~/projects/`, e.g.

```bash
mkdir -p ~/projects/ && cd ~/projects/
git clone https://gitlab.com/theepic-dev/ansible-epic-dev
cd ansible-epic-dev
```

Edit the `inventory` file, and define the roles you want to use (`nvim inventory`).

Create a `vars/globals.yml` file and define the required variables. You can use the `vars/globals.tpl.yml` file as a template:

```bash
cp -iv vars/globals.tpl.yml vars/globals.yml
nvim vars/globals.yml
```

Finally, run the playbook:

```bash
ansible-playbook -K main.yml
```

### nvim plugin installation

You can run `nvim -u ~/.config/nvim/packersync.lua` to automatically install plugins and prevent nvim from trying to use them prior to their installation.

In fact, it should now be even simpler - simply run `nsync` from a `fish` shell, as we have a nice fish alias for it :D

## Caveats

- The nerd font is installed in the GUI role, but the Vimmer role depends on it. If you ever need a console only install, fix this.

## Window/pane navigation

You can jump between nvim windows and/or tmux panes using the Control key and `h`, `j`, `k`, `l`, with the same directions as in neovim itself (left, down, up, right, respectively).

You can jump from to the previous/next tmux window using the Alt key and a left/right arrow. My personal preference is to open a new tmux session for every project I am working on, running nvim in the first window, and one or more terminal panes in the second window of each session, as the project dictates. When using `docker compose`, running multiple commands inside different containers, and `git`, you can use multiple panes to manage everything.

## nvim cheat sheet

Here you can find some defined shortcuts for the installed plugins.

**Note**: `<leader>` is mapped to `<space>`.

### General

Use `<leader><leader>` or `<space><space>` to toggle code folds. Note that it's weird with TreeSitter, and folding won't happen at one level at a time, but all levels. To be investigated!

Use `<leader>?` to see the capture group under the cursor (great for theming).

### Debugging (DAP)

| Mode | Shortcut    | Feature                                                                    |
| ---- | ----------- | -------------------------------------------------------------------------- |
| `n`  | `<leader>d` | Pressing `<leader>d` will open which-key, which shows all defined mappings |
| `n`  | `<F5>`      | Start/Continue Debugging                                                   |
| `n`  | `<F8>`      | Step over function or method                                               |
| `n`  | `<F9>`      | Step into function or method (if possible)                                 |
| `n`  | `<F10>`     | Step out of function or method                                             |

**Note**: for debugging inside docker containers, create a `launch.json` file and launch the remote connection with `<leader>dl`.

### Telescope

| Mode | Shortcut    | Feature                                                                  |
| ---- | ----------- | ------------------------------------------------------------------------ |
| `n`  | `<leader>f` | Pressing <leader>f will open which-key, which shows all defined mappings |
| `n`  | `<C-/>`     | Fuzzy find current buffer                                                |

Other things worth considering:

- `colorscheme`
- `command_history`
- `filetypes`
- `g` everything... a lot of git stuff
- `jumplist`
- `l...` lsp stuffs
- `search_history` to search through old searches... `<leader>/`?

### LSP

| Mode | Shortcut | Feature                |
| ---- | -------- | ---------------------- |
| `n`  | `gN`     | Diagnostics: goto prev |
| `n`  | `gP`     | Diagnostics: goto next |
| `n`  | `K`      | Hover definition       |

### LuaSnip

| Mode     | Shortcut | Feature               |
| -------- | -------- | --------------------- |
| `i`, `s` | `<C-k>`  | Jump to next field    |
| `i`, `s` | `<C-j>`  | Jump to prev field    |
| `i`      | `<C-l>`  | Choose next selection |

### Neotest

| Mode | Shortcut | Feature                                           |
| ---- | -------- | ------------------------------------------------- |
| `n`  | `tds`    | Show diagnostics (May be borked right now - TODO) |
| `n`  | `tdh`    | Hide diagnostics (May be borked right now - TODO) |
| `n`  | `tf`     | Run current test file                             |
| `n`  | `to`     | Open tests output window                          |
| `n`  | `tr`     | Run test under cursor                             |
| `n`  | `ts`     | Toggle summary window                             |
| `n`  | `ttw`    | Toggle watching current test file                 |

### nvim troubleshooting

- Run `:checkhealth` to diagnose any errors.
- If some TreeSitter parsers list errors, reinstall them with `:TSInstall [parser]`. They often break after plugin updates.
- If you bork `nvim` entirely, run it as `nvim -u NONE` which skips loading the config entirely. You can then use `nvim` to edit whatever file you messed up.

## Extra configuration

### DOSBox

Modify the `~/.dosbox/dosbox-0.74-3.conf` file and set the following options for MIDI to work properly:

```conf
mididevice=fluidsynth
midiconfig=128:0
```

Optionally, add the following lines to the `[autoexec]` section to make things easier (assuming your DOS games are stored in `~/.games/dos/`, of course):

```conf
[autoexec]
mount c ~/.games/dos/
c:
```
