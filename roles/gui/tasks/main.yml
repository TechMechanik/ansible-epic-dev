---
# GUI apps
# vi: ft=yaml.ansible

- name: Create required directories
  ansible.builtin.file:
    name: "{{ item }}"
    state: directory
    mode: 0700
  loop:
    - "{{ user_home_dir }}/.config/fontconfig/conf.d/"
    - "{{ user_home_dir }}/.config/autostart/"
    - "{{ user_home_dir }}/.local/share/applications/"

- name: Create required system directories
  become: true
  ansible.builtin.file:
    name: "{{ item }}"
    state: directory
    mode: 0755
  loop:
    - /etc/systemd/system/bluetooth.service.d/

# Enable some non-standard bluetooth commands to read battery status from headphones that use custom protocols
- name: Enable experimental bluetooth features
  become: yes
  ansible.builtin.copy:
    src: files/bluez-experiment.conf
    dest: /etc/systemd/system/bluetooth.service.d/
    mode: 0644


- name: Install common GUI apps
  become: yes
  community.general.pacman:
    name:
      - firefox
      - flatpak
      - keepassxc
      - plasma-meta
      - signal-desktop
      - systray-x-kde
      - thunderbird
      - yakuake
    state: present

- name: Add flathub repo
  community.general.flatpak_remote:
    method: user
    name: flathub
    flatpakrepo_url: https://dl.flathub.org/repo/flathub.flatpakrepo
    state: present

- name: Install flatpak apps
  community.general.flatpak:
    method: user
    name:
      - com.skype.Client
      - com.discordapp.Discord
      - com.slack.Slack
    state: present
  ignore_errors: true

- name: Autostart yakuake
  ansible.builtin.copy:
    src: files/autostart_org.kde.yakuake.desktop
    dest: "{{ user_home_dir }}/.config/autostart/org.kde.yakuake.desktop"
    mode: 0700

- name: Install fonts
  ansible.builtin.copy:
    src: "files/fonts/{{ item }}"
    dest: "{{ user_home_dir }}/.local/share/fonts/"
    mode: 0644
  loop:
    - HackNerdFontMono-Regular.ttf
    - NotoSansMNerdFontMono-Regular.ttf

- name: Install Epic.profile for konsole
  ansible.builtin.copy:
    src: files/Epic.profile
    dest: "{{ user_home_dir }}/.local/share/konsole/"
    force: no
    mode: 0700

- name: Install fontconfig files
  ansible.builtin.copy:
    src: "files/fontconfigs/{{item }}"
    dest: "{{ user_home_dir }}/.config/fontconfig/conf.d/"
    mode: 0700
  loop:
    - /50-disable_ligatures.conf

- name: Install .desktop files
  ansible.builtin.copy:
    src: "files/applications/{{ item }}"
    dest: "{{ user_home_dir }}/.local/share/applications/"
    mode: 0600
  loop:
    - forgetful-firefox.desktop
    - signal-tray.desktop
