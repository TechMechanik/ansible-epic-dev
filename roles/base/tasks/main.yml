---
# Common to all roles
# vi: ft=yaml.ansible

- name: Enable timesyncd
  become: yes
  ansible.builtin.systemd:
    name: systemd-timesyncd.service
    state: started
    enabled: yes

- name: Install common software
  become: yes
  community.general.pacman:
    name:
      - bat
      - bind
      - cowsay
      - direnv
      - fish
      - glow
      - htop
      - ipcalc
      - noto-fonts
      - noto-fonts-cjk
      - noto-fonts-emoji
      - openssh
      - pacman-contrib
      - rsync
      - sshfs
      - tree
      - unrar
      - unzip
      - usbutils
      - which
      - whois

- name: Enable paccache cleanup timer
  become: yes
  ansible.builtin.systemd:
    name: paccache.timer
    state: started
    enabled: yes

- name: Configure pacman
  become: yes
  ansible.builtin.lineinfile:
    path: /etc/pacman.conf
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
  loop:
    - { regexp: "^(#)?Color", line: "Color" }
    - { regexp: "^(#)?VerbosePkgLists", line: "VerbosePkgLists" }
    - { regexp: "^(#)?ParallelDownloads", line: "ParallelDownloads = 3" }

- name: Create required directories
  ansible.builtin.file:
    name: "{{ item }}"
    state: directory
    mode: 0700
  loop:
    - "{{ user_home_dir }}/.config/fish/conf.d/"
    - "{{ user_home_dir }}/.ssh/"

- name: Add ~/.local/bin to PATH
  ansible.builtin.copy:
    src: files/home_local_bin.fish
    dest: "{{ user_home_dir }}/.config/fish/conf.d/"
    mode: 0644

- name: Add OpenSSH config file
  ansible.builtin.copy:
    src: files/ssh_config
    dest: "{{ user_home_dir }}/.ssh/config"
    mode: 0600
    force: false

- name: Add file_ext function to fish
  ansible.builtin.copy:
    src: files/fish/file_ext.fish
    dest: "{{ user_home_dir }}/.config/fish/functions/"
    mode: 0644

- name: Add cat function to fish
  ansible.builtin.copy:
    src: files/fish/cat.fish
    dest: "{{ user_home_dir }}/.config/fish/functions/"
    mode: 0644

- name: Copy fisher install script
  ansible.builtin.copy:
    src: files/fish/fisher.fish
    dest: /tmp/
    mode: 0644
  when: want_fisher

- name: Install fisher
  ansible.builtin.command:
    cmd: /usr/bin/fish -c "source /tmp/fisher.fish && fisher install jorgebucaran/fisher"
    creates: "{{ user_home_dir }}/.config/fish/fish_plugins"
  when: want_fisher

- name: Install tide prompt
  ansible.builtin.command:
    cmd: /usr/bin/fish -c "fisher install IlanCosman/tide@v5"
    creates: "{{ user_home_dir }}/.config/fish/functions/tide.fish"
  when: want_fish_tide

- name: Add direnv support to fish
  ansible.builtin.copy:
    src: files/fish/direnv.fish
    dest: "{{ user_home_dir }}/.config/fish/conf.d/"
    mode: 0644
