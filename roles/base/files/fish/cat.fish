function cat --wraps=/usr/bin/bat --description 'Calls bat for normal files, cat for no args, or glow for *.md files.'
    set -f argc (count $argv)
    if test $argc -eq 0
        # No filename passed; use system cat
        set -f cmd cat
    else
        # Count how many markdown and other files there are
        set -l md_files 0
        set -l other_files 0
        for arg in $argv
            # Get the first character of the arg so we can skip those beginning with -
            set -l prefix (echo $arg | cut -c -1)
            if test $prefix != -
                # Get the file extension
                set -l ext (file_ext $arg)
                if test $ext = md -o $ext = MD
                    set md_files (math $md_files + 1)
                else
                    set other_files (math $other_files + 1)
                end
            end
        end

        # Choose between bat and glow
        if test $md_files -gt 0 -a $other_files -eq 0
            # One or more md files; no other files
            set -f cmd glow
        else
            # Some non-md files; use bat
            set -f cmd bat
        end
        # echo $md_files markdown files, $other_files other files.
    end
    command $cmd $argv
end
