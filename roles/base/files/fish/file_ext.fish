function file_ext --description 'Prints the extension for a given filename.'
    if test (count $argv) -ne 1
        echo $argv
    else
        set -l file_parts (string split . $argv[1])
        if test (count $file_parts) -eq 1
            echo $argv
        else
            # Print only the last part
            echo $file_parts[(count $file_parts)]
        end
    end
end
