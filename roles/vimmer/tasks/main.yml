---
# Neovim config
# vi: ft=yaml.ansible
# yamllint disable rule:truthy rule:octal-values

- name: Install required packages
  become: yes
  community.general.pacman:
    name:
      - ansible-lint
      - fd
      - flake8
      - gcc
      - glab
      - go
      - go-tools
      - gopls
      - hex
      - jq
      - libc++
      - lua-language-server
      - mypy
      - neovim
      - npm
      - powerline
      - powerline-fonts
      - prettier
      - python-argon2_cffi
      - python-debugpy
      - python-django
      - python-django-debug-toolbar
      - python-lsp-black
      - python-lsp-server
      - python-passlib
      - python-pip
      - python-poetry
      - python-pynvim
      - python-pytest
      - python-pytest-cov
      - python-pytest-django
      - python-pytest-ruff
      - python-rope
      - python-ruff
      - ripgrep
      - ruff
      - ruff-lsp
      - tmux
      - tmuxp
      - tree-sitter
      - tree-sitter-cli
      - typescript-language-server
      - yaml-language-server

- name: Configure git email
  community.general.git_config:
    name: user.email
    scope: global
    value: "{{ git_email }}"

- name: Configure git name
  community.general.git_config:
    name: user.name
    scope: global
    value: "{{ git_name }}"

- name: Configure git default branch
  community.general.git_config:
    name: init.defaultBranch
    scope: global
    value: main

- name: Install pip dependencies
  ansible.builtin.pip:
    name:
      - celery-stubs
      - confluent-kafka-stubs
      - django-cookie-consent
      - django-stubs[compatible-mypy]
      - djangorestframework-stubs[compatible-mypy]
      - djlint
      - dotpla
      - flake8-django
      - mixer
      - mkdocs
      - model_bakery
      - pylsp-mypy
      - pylsp-rope
      - pymongo-stubs
      - strawberry-graphql-django
      - types-Flask
      - types-Pygments
      - types-requests
      - wagtail
    state: latest
    # PEP 668 is stupid, so I gotta use the break-system-packages flag.
    # It shouldn't break anything, as those packages are not installable via pacman, though that may change in the future, of course.
    extra_args: --user --break-system-packages

- name: Create required directories
  ansible.builtin.file:
    name: "{{ item }}"
    state: directory
    mode: 0700
  loop:
    - "{{ user_home_dir }}/.local/epic_npm"
    - "{{ user_home_dir }}/.config/nvim/colors/"
    - "{{ user_home_dir }}/.config/nvim/lua/epic/theme/"
    - "{{ user_home_dir }}/.config/nvim/lua/lualine/themes"

- name: Install or update NPM packages
  community.general.npm:
    name: "{{ item }}"
    path: "{{ user_home_dir }}/.local/epic_npm"
    state: latest
  loop:
    - "@ansible/ansible-language-server"
    - dockerfile-language-server-nodejs
    - emmet-ls
    - "@tailwindcss/language-server"
    - prettier-eslint
    - prettier-plugin-tailwindcss
    - "@vue/language-server"

- name: Config init.lua
  ansible.builtin.copy:
    src: files/init.lua
    dest: "{{ user_home_dir }}/.config/nvim/"
    mode: 0644

- name: Add packersync.lua file just to install plugins
  ansible.builtin.copy:
    src: files/packersync.lua
    dest: "{{ user_home_dir }}/.config/nvim/"
    mode: 0644

- name: Add fish nsync command to sync nvim plugins
  ansible.builtin.copy:
    src: files/nsync.fish
    dest: "{{ user_home_dir }}/.config/fish/functions/"
    mode: 0644

- name: Copy nvim/lua/epic/ files
  ansible.builtin.copy:
    src: "files/epic/{{ item }}"
    dest: "{{ user_home_dir }}/.config/nvim/lua/epic/"
    mode: 0644
  loop:
    - cmp.lua
    - dap.lua
    - gitsigns.lua
    - globals.lua
    - lualine.lua
    - luasnip.lua
    - neotest.lua
    - notify.lua
    - nvim_tmux.lua
    - plugins.lua
    - telescope.lua
    - treesitter.lua
    - which_key.lua

- name: Copy epic/lsp.lua from template
  ansible.builtin.template:
    src: templates/lsp.lua
    dest: "{{ user_home_dir }}/.config/nvim/lua/epic/lsp.lua"
    mode: 0644

# All the LuaSnip snippets
- name: Install LuaSnip snippets
  ansible.builtin.copy:
    src: "files/snippets/{{ item }}"
    dest: "{{ user_home_dir }}/.config/nvim/lua/snippets/"
    mode: 0644
  loop:
    - cfg.lua
    - html.lua
    - htmldjango.lua
    - lua.lua
    - python.lua
    - toml.lua

# Theme installation
- name: Install colors/epic.lua
  ansible.builtin.copy:
    src: files/colors/epic.lua
    dest: "{{ user_home_dir }}/.config/nvim/colors/epic.lua"
    mode: 0644

- name: Install theme/init.lua
  ansible.builtin.copy:
    src: files/epic/theme/init.lua
    dest: "{{ user_home_dir }}/.config/nvim/lua/epic/theme/init.lua"
    mode: 0644

- name: Install theme/definitions.lua
  ansible.builtin.copy:
    src: files/epic/theme/definitions.lua
    dest: "{{ user_home_dir }}/.config/nvim/lua/epic/theme/definitions.lua"
    mode: 0644

- name: Install lualine theme
  ansible.builtin.copy:
    src: files/epic/theme/lualine.epic.lua
    dest: "{{ user_home_dir }}/.config/nvim/lua/lualine/themes/epic.lua"
    mode: 0644

# Tmux keybindings are life
- name: Setup tmux.conf
  ansible.builtin.copy:
    src: files/tmux.conf
    dest: "{{ user_home_dir }}/.config/tmux/"
    mode: 0644

- name: Clone tmuxp layouts
  when: user_is_epic
  ansible.builtin.git:
    accept_newhostkey: true
    dest: "{{ user_home_dir }}/.config/tmuxp/"
    repo: git@gitlab.com:theepic-dev/tmuxp.git
    update: false
