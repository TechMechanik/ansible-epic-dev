-- Tmux-friendly keybindings

local tmux = require "nvim-tmux-navigation"

tmux.setup {
  keybindings = {
    left = "<c-h>",
    down = "<c-j>",
    up = "<c-k>",
    right = "<c-l>",
    last_active = [[<c-\>]],
    next = "<c-space>",
  }
}
