local ls = require "luasnip"

-- LuaSnip config
ls.config.set_config {
  -- Remember the last edited snippet so we can edit it if we navigate away from it
  history = true,

  -- Enable dynamic snippets
  updateevents = "TextChanged,TextChangedI",

  enable_autosnippets = true,
}

-- Key bindings
-- Jump to next field if available
vim.keymap.set({ "i", "s" }, "<c-k>", function()
  if ls.expand_or_jumpable() then
    ls.expand_or_jump()
  end
end, { silent = true })
-- Jump to prev field if available
vim.keymap.set({ "i", "s" }, "<c-j>", function()
  if ls.jumpable(-1) then
    ls.jump(-1)
  end
end, { silent = true })
-- Cycle through list of options
vim.keymap.set("i", "<c-l>", function()
  if ls.choice.active() then
    ls.change_choice(1)
  end
end)

-- Load snippets from lua files
require("luasnip.loaders.from_lua").load({ paths = "~/.config/nvim/lua/snippets/" })
