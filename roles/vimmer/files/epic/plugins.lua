-- lazy.nvim plugins

require("lazy").setup({
  -- AI helpers
  {
    "Exafunction/codeium.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "hrsh7th/nvim-cmp",
    },
    config = function()
      require("codeium").setup({
      })
    end
  },

  -- AUTOCOMPLETION
  "hrsh7th/nvim-cmp",
  "hrsh7th/cmp-nvim-lsp",
  "hrsh7th/cmp-nvim-lua",
  "hrsh7th/cmp-buffer",
  "hrsh7th/cmp-path",
  "saadparwaiz1/cmp_luasnip",

  -- DEBUGGING
  -- Debugging Adapter Protocol and its adapters
  "mfussenegger/nvim-dap",
  "mfussenegger/nvim-dap-python",
  -- Lua adapter
  "jbyuki/one-small-step-for-vimkind",
  {
    "rcarriga/nvim-dap-ui",
    dependencies = {
      "mfussenegger/nvim-dap",
      "nvim-neotest/nvim-nio",
    },
  },
  {
    "theHamsta/nvim-dap-virtual-text",
    dependencies = { "mfussenegger/nvim-dap" },
  },

  -- DOCKER integration
  "git@gitlab.com:theepic-dev/docker-aware.nvim.git",

  -- FUZZY SEARCH
  {
    "nvim-telescope/telescope.nvim",
    branch = "0.1.x",
    dependencies = {
      { "nvim-lua/plenary.nvim" }
    },
  },
  { "nvim-telescope/telescope-file-browser.nvim" },

  -- GIT
  -- Git blame to see file history
  "f-person/git-blame.nvim",
  -- Show git diffs
  { "sindrets/diffview.nvim",                    dependencies = "nvim-lua/plenary.nvim" },
  -- Display git info in left column plus other features
  "lewis6991/gitsigns.nvim",

  -- INPUT / UI
  -- Color picker
  "uga-rosa/ccc.nvim",
  -- Add icons to nvim-cmp / LSP
  "onsails/lspkind.nvim",
  -- Status line
  "nvim-lualine/lualine.nvim",
  -- Better notifications
  "rcarriga/nvim-notify",
  -- Disable peasant navigation
  "alexghergh/nvim-tmux-navigation",
  -- Web dev icons for files
  "nvim-tree/nvim-web-devicons",
  -- Show keystroke sequences
  "folke/which-key.nvim",
  -- Cool themes, hopefully
  "i3d/vim-jimbothemes",

  -- LSP
  -- Configuration for different language servers
  "neovim/nvim-lspconfig",
  -- Signatures that provide argument info for functions and classes
  "ray-x/lsp_signature.nvim",
  -- LSP UI improvements
  {
    "nvimdev/lspsaga.nvim",
    dependencies = {
      "nvim-tree/nvim-web-devicons",
      "nvim-treesitter/nvim-treesitter",
    },
  },
  -- For formatters and linters that don't work with LSP
  "mhartington/formatter.nvim",
  "mfussenegger/nvim-lint",

  -- NEOVIM
  "folke/neodev.nvim",

  -- NOTE Taking
  {
    "nvim-neorg/neorg",
    lazy = false, -- specify lazy = false because some lazy.nvim distributions set lazy = true by default
    -- tag = "*",
    dependencies = {
      "luarocks.nvim",
    },
    config = function()
      require("neorg").setup {
        load = {
          ["core.defaults"] = {},  -- Loads default behaviour
          ["core.concealer"] = {}, -- Adds pretty icons to your documents
          ["core.dirman"] = {      -- Manages Neorg workspaces
            config = {
              workspaces = {
                notes = "~/Documents/notes",
              },
            },
          },
        },
      }
    end,
  },

  -- Package management
  {
    "vhyrro/luarocks.nvim",
    priority = 1000,
    config = true,
  },

  -- SNIPPETS
  "L3MON4D3/LuaSnip",

  -- SYNTAX HIGHLIGHTING
  -- Extra syntax queries
  "https://gitlab.com/theepic-dev/epichi.git",
  -- Tree-sitter for better syntax highlighting
  {
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
  },
  -- Tree-sitter playground to show highlight groups
  "nvim-treesitter/playground",

  -- Test integration
  {
    "nvim-neotest/neotest",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-treesitter/nvim-treesitter",
      -- runner support
      "nvim-neotest/neotest-plenary",
      "nvim-neotest/neotest-python",
    },
  },
})
