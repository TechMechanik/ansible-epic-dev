-- WhichKey config
local wk = require("which-key")

-- PLugins we will be using
local dap = require("dap")
local dapui = require("dapui")
local nt = require("neotest")
local telescope = require("telescope")
local builtin = require("telescope.builtin")

-- Import my own modules
local epic_dap = require("epic.dap")

-- Override default settings below if needed
wk.setup {}

-- WhichKey options
local opts = {
  prefix = "<leader>",
}

-- Define mappings here
local mappings = {
  c = {
    name = "Color Picker",
    c = { "<cmd>CccConvert<cr>", "C)onvert C)olor" },
    h = { "<cmd>CccHighlighterEnable<cr>", "Enable C)olor H)ighlighting" },
    H = { "<cmd>CccHighlighterDisable<cr>", "Disable C)olor H)ighlighting" },
    p = { "<cmd>CccPick<cr>", "Open C)olor P)icker" },
    t = { "<cmd>CccHighlighterToggle<cr>", "C)olor Highlighting T)oggle" },
  },
  d = {
    name = "DAP",
    b = { dap.toggle_breakpoint, "D)ebugger B)reakpoint toggle" },
    c = { function() dap.set_breakpoint(vim.fn.input("Condition: ")) end, "D)ebugger C)onditional breakpoint" },
    l = { epic_dap.dap_launch_from_json, "L)aunch from launch.json" },
    L = {
      name = "Lua",
      o = { function() require("osv").launch({ port = 8086 }) end, "Launch O)SV" },
      O = { function() require("osv").stop() end, "Stop O)SV" },
    },
    u = {
      name = "UI",
      t = { dapui.toggle, "T)oggle" },
    },
  },
  f = {
    name = "Fuzzy search (telescope)",
    b = { telescope.extensions.file_browser.file_browser, "F)ile B)rowser" },
    c = { builtin.commands, "F)ind C)ommands" },
    d = { builtin.diagnostics, "F)ind D)iagnostics" },
    f = { builtin.find_files, "F)ind F)iles" },
    g = { builtin.live_grep, "F)ind live G)rep" },
    h = { builtin.help_tags, "F)ind H)elp tags" },
    H = { builtin.highlights, "F)ind H)ighlights" },
    k = { builtin.keymaps, "F)ind K)eymaps" },
    r = { builtin.lsp_references, "F)ind LSP R)eferences" },
    u = { builtin.buffers, "F)ind b{U}ffers" },
  },
  l = {
    name = "LSP",
    a = { "<cmd>Lspsaga code_action<cr>", "L)SP Code A)ction" },
    o = { "<cmd>Lspsaga outline<cr>", "L)SP O)utline" },
    p = { "<cmd>Lspsaga peek_definition<cr>", "L)SP P)eek definition" },
  },
  t = {
    name = "NeoTest",
    f = { function() nt.run.run(vim.fn.expand("%")) end, "Run F)ile" },
    o = { nt.output_panel.toggle, "Open" },
    r = { nt.run.run, "Run" },
    s = { nt.summary.toggle, "Toggle S)ummary" },
    w = { function() nt.watch.toggle(vim.fn.expand("%")) end, "Watch current file" },
  },
  T = {
    name = "Toggle...",
    s = { function() vim.opt_local.spell = not vim.opt_local.spell:get() end, "Spellcheck" },
  },
  x = { "<cmd>source %<cr", "Source current file<cr>" },
}

wk.register(mappings, opts)
