-- nvim-cmp config

local lspkind = require "lspkind"
lspkind.init()

local cmp = require "cmp"

cmp.setup({
  mapping = cmp.mapping.preset.insert({
    ["<C-d>"] = cmp.mapping.scroll_docs(-4),
    ["<C-f>"] = cmp.mapping.scroll_docs(4),
    ["<C-Space>"] = cmp.mapping.complete(),
    ["<C-e>"] = cmp.mapping.abort(),
    ["<CR>"] = cmp.mapping.confirm({ select = true }),
  }),
  sources = cmp.config.sources({
    { name = "codeium" },
    { name = "luasnip" },
    { name = "nvim_lsp" },
    { name = "nvim_lua" },
    { name = "buffer",  keyword_length = 4 },
    { name = "path" },
  }),
  snippet = {
    expand = function(args)
      require("luasnip").lsp_expand(args.body)
    end,
  },
  formatting = {
    format = lspkind.cmp_format {
      menu = {
        buffer = "[buf]",
      },
      mode = "symbol_text",
      symbol_map = {
        Codeium = "",
      },
    },
  },
  experimental = {
    ghost_text = true,
  },
})
