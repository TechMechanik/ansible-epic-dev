-- Git signs config. This is very basic, so see the git repo for other options
-- https://github.com/lewis6991/gitsigns.nvim

require("gitsigns").setup {}
