-- Neotest config

local nt = require("neotest")

local dockeraware = require("dockeraware")

local da_config = {
  setup_project = true,
  -- The config that will be passed to the dockeraware adapter
  adapter_config = {
    translate_path = true,
    extra_args = "",
  },
  verbose = true,
}

nt.setup({
  adapters = {
    require("neotest-python"),
    require("neotest-plenary"),
  },
  quickfix = {
    open = false,
  },
  -- log_level = "debug",
})

dockeraware.setup(da_config)
