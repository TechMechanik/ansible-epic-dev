-- Theme colors definition
local colors = {
  blue_baby = "#89cff0",
  blue_bdazzled = "#2e5894",
  blue_bdazzled_muted = "#22426f",
  blue_cornflower = "#6969ff",
  blue_flashy = "#3cf0f0",
  blue_light = "#00caa0",
  blue_light_muted = "#409778",
  blue_med_dark = "#00008b",
  blue_mid = "#003280",
  blue_mid_muted = "#002560",
  blue_med_dark_muted = "#002040",
  black = "#000000",
  green_django = "#0c4b33",
  green_green = "#00ff00",
  green_highlight = "#60ff00",
  green_muted = "#008000",
  grey_dark = "#101010",
  grey_gainsboro = "#dcdcdc",
  grey_gainsboro_muted = "#b9b9b9",
  grey = "#808080",
  grey_light_green = "#75b175",
  grey_med_dark = "#333333",
  grey_silver = "#c0c0c0",
  grey_x11 = "#bebebe",
  orange = "#ffa500",
  orange_muted = "#c06800",
  pink_amaranth = "#f19cbb",
  pink_amaranth_muted = "#b4758c",
  pink_barbie = "#da1884",
  pink_barbie_muted = "#a31263",
  pink_bubblegum = "#ffc1cc",
  pink_hot = "#ff69b4",
  pink_hot_muted = "#bf4e87",
  pink_salmon = "#ffa0a0",
  pink_salmon_muted = "#dd8888",
  purple = "#500050",
  purple_muted = "#400040",
  purple_complement_green_green = "#ff00ff",
  red_fiery = "#c00000",
  red_muted = "#c03333",
  yellow_arctic_lime = "#d0ff14",
  yellow_arctic_lime_muted = "#9cbf0f",
  yellow_electric_lime = "#ccff00",
  yellow_french_lime = "#9efd38",
  yellow_laser_lemon = "#ffff60",
  yellow_laser_lemon_muted = "#bfbf48",
  yellow_light = "#dadb00",
  yellow_muted = "#969600",
}

local color_vals = {
  black_on_blue = { fg = colors.black, bg = colors.blue_light },
  black_on_yellow = { fg = colors.black, bg = colors.yellow_light, bold = true },
  pink_on_blue = { fg = colors.pink_hot, bg = colors.blue_baby, bold = true },
  hot_pink_on_cf_blue = { fg = colors.pink_hot, bg = colors.blue_cornflower, bold = true },
}

local groups = {
  -- No decorations
  Normal = { fg = colors.green_muted, bg = "NONE", bold = false, italic = false },
  -- Base
  Boolean = { fg = colors.yellow_french_lime, italic = true },
  Conditional = { fg = colors.yellow_laser_lemon_muted, bold = false },
  Constant = { fg = colors.pink_salmon_muted },
  Comment = { fg = colors.grey },
  CursorLine = { bg = colors.grey_light_green, fg = colors.black },
  DiffAdd = { bg = colors.blue_med_dark_muted },
  Folded = { fg = colors.grey_x11, bg = "NONE", bold = true, italic = true },
  Identifier = { fg = colors.blue_bdazzled_muted, bold = false },
  Include = { fg = colors.purple_muted, bold = false },
  NormalFloat = { bg = colors.grey_dark, blend = 5 },
  Pmenu = { fg = colors.black, bg = colors.green_green },
  Repeat = { fg = colors.yellow_laser_lemon_muted, bold = false },
  PmenuSel = { fg = colors.green_green, bg = colors.black, bold = true },
  TabLine = { fg = colors.green_highlight, bg = colors.grey_med_dark },
  Type = { fg = colors.orange_muted, bold = false },
  -- Custom highlight groups
  ["@EpicImportFromModule"] = { fg = colors.pink_amaranth_muted },
  ["@EpicImportModule"] = { fg = colors.pink_hot_muted, bg = colors.blue_light_muted, bold = false },
  -- nvim-cmp
  CmpItemAbbrMatch = { bold = true },
  CmpItemKind = { fg = colors.purple_complement_green_green, bold = true, italic = true },
  -- nvim-notify
  NotifyBackground = { bg = colors.black },
  TelescopeSelection = { reverse = true },
  -- Tree Sitter
  -- ["@constant.builtin"] = { bg = colors.yellow_light, fg = colors.black },
  ["@field"] = { fg = colors.blue_light },
  ["@function"] = { fg = colors.yellow_muted },
  ["@keyword"] = { fg = colors.red_muted, bold = false },
  ["@number"] = { fg = colors.pink_hot, bold = false },
  ["@operator"] = { fg = colors.yellow_muted, bold = false },
  ["@punctuation"] = { fg = colors.orange_muted, bold = false },
  ["@punctuation.bracket"] = { fg = colors.yellow_arctic_lime_muted, bold = false},
  -- ["@operator.variable"] = { fg = colors.yellow_light, bold = true, italic = true, underline = true },
  ["@preproc"] = { fg = colors.blue_light },
  ["@string"] = { fg = colors.green_muted, bold = true, italic = true },
  ["@variable"] = { fg = colors.pink_barbie_muted, bold = false},
  -- By default, it lnks to Identifier which should be good enough for our purposes.
  -- ["@variable"] = { fg = colors.pink_hot },
  -- Language specific
  djangoVarBlock = { fg = colors.black, bg = colors.green_django, bold = true },
}

local win_active = {
  -- Overrides for active windows
  Normal = { fg = colors.green_green, bg = "NONE", bold = false, italic = false },
  Boolean = { fg = colors.yellow_electric_lime, bold = true, italic = true },
  Conditional = { fg = colors.yellow_laser_lemon, bold = true },
  Constant = { fg = colors.pink_salmon },
  Comment = { fg = colors.grey_silver },
  CursorLine = { bg = colors.green_highlight, fg = colors.black },
  DiffAdd = { bg = colors.blue_med_dark },
  Identifier = { fg = colors.blue_bdazzled, bold = true },
  Include = { fg = colors.purple, bold = true },
  Repeat = { fg = colors.yellow_laser_lemon, bold = true },
  Type = { fg = colors.orange, bold = true },
  ["@EpicImportFromModule"] = { fg = colors.pink_amaranth },
  ["@EpicImportModule"] = color_vals.hot_pink_on_cf_blue,
  ["@field"] = { fg = colors.blue_flashy },
  ["@function"] = { fg = colors.yellow_light },
  ["@keyword"] = { fg = colors.red_fiery, bold = true },
  ["@number"] = { fg = colors.pink_bubblegum, bold = true },
  ["@operator"] = { fg = colors.yellow_light, bold = true },
  ["@punctuation"] = { fg = colors.orange, bold = true },
  ["@punctuation.bracket"] = { fg = colors.yellow_arctic_lime, bold = true},
  ["@string"] = { fg = colors.green_highlight, bold = true, italic = true },
  ["@variable"] = { fg = colors.pink_barbie, bold = true},
}

-- Copy the active table to insert, and override CursorLine
local win_active_insert = {}
for g, v in pairs(win_active) do
  win_active_insert[g] = v
end
win_active_insert.CursorLine = { fg = "NONE", bg = "NONE", bold = false, italic = false }

local theme = {
  colors = colors,
  groups = groups,
  win_active = win_active,
  win_active_insert = win_active_insert,
}
return theme
