-- epic theme for lualine. This requires epic theme from
-- https://gitlab.com/theepic-dev/ansible-epic-dev

local definitions = require("epic.theme.definitions")
local colors = definitions.colors

return {
  normal = {
    a = { fg = colors.black, bg = colors.green_green, gui = "bold" },
    b = { fg = colors.black, bg = colors.orange },
    c = { fg = colors.grey_dark, bg = colors.grey_silver },
  },
  insert = {
    a = { fg = colors.black, bg = colors.pink_barbie, gui = "bold" },
    b = { fg = colors.black, bg = colors.yellow_electric_lime },
    c = { fg = colors.grey_dark, bg = colors.grey_silver, gui = "bold" },
  },
  visual = {
    a = { fg = colors.grey_gainsboro, bg = colors.purple, gui = "bold" },
    b = { fg = colors.black, bg = colors.pink_hot },
    c = { fg = colors.grey_dark, bg = colors.grey_silver },
  },
  replace = {
    a = { fg = colors.grey_gainsboro, bg = colors.red_fiery, gui = "bold" },
    b = { fg = colors.black, bg = colors.pink_amaranth },
    c = { fg = colors.grey_dark, bg = colors.grey_silver, gui = "bold" },
  },
  command = {
    a = { fg = colors.grey_gainsboro, bg = colors.blue_cornflower, gui = "bold" },
    b = { fg = colors.grey_gainsboro, bg = colors.blue_light_muted },
    c = { fg = colors.grey_dark, bg = colors.grey_silver },
  },
  inactive = {
    a = { fg = colors.grey_silver, bg = colors.green_muted },
    b = { fg = colors.black, bg = colors.orange_muted },
    c = { fg = colors.grey_dark, bg = colors.grey },
  },
}
