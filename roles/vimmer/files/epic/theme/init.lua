-- Exported module
local M = {}

M.config = {
  -- Default config
}

M.load = function()
  if vim.version().minor < 8 then
    vim.notify_once("epic colors theme requires neovim 0.8 or greater")
    return
  end

  -- Reset colors
  if vim.g.colors_name then
    vim.cmd([[
      hi clear
      syntax reset
      ]])
  end

  vim.g.colors_name = "epic"
  vim.opt.background = "dark"

  -- Enable transparency on popup menus
  vim.o.pumblend = 12

  -- Import color definitions
  local definitions = require("epic.theme.definitions")
  local groups = definitions.groups
  local win_active_groups = definitions.win_active
  local win_active_insert_groups = definitions.win_active_insert

  -- Define namepaces to apply different themes
  local ns_win_active = vim.api.nvim_create_namespace("window_active")
  local ns_win_active_insert = vim.api.nvim_create_namespace("window_active_insert")

  -- Set main theme colors
  for group, settings in pairs(groups) do
    vim.api.nvim_set_hl(0, group, settings)
  end

  -- Set colors for active windows
  for group, settings in pairs(win_active_groups) do
    vim.api.nvim_set_hl(ns_win_active, group, settings)
  end

  -- Set colors for active windows in insert mode
  for group, settings in pairs(win_active_insert_groups) do
    vim.api.nvim_set_hl(ns_win_active_insert, group, settings)
  end

  -- Set highlight namespace based on Events
  vim.api.nvim_create_autocmd({ "BufEnter", "WinEnter" }, {
    callback = function()
      vim.api.nvim_win_set_hl_ns(0, ns_win_active)
    end
  })
  vim.api.nvim_create_autocmd({ "BufLeave", "WinLeave" }, {
    callback = function()
      vim.api.nvim_win_set_hl_ns(0, 0)
    end
  })

  -- Toggle cursor line colors based on mode
  vim.api.nvim_create_autocmd("InsertEnter", {
    callback = function()
      vim.api.nvim_win_set_hl_ns(0, ns_win_active_insert)
    end
  })
  vim.api.nvim_create_autocmd("InsertLeave", {
    callback = function()
      vim.api.nvim_win_set_hl_ns(0, ns_win_active)
    end
  })
end

return M
