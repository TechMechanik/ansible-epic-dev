-- Telescope config
-- Imports
local telescope = require("telescope")
local builtin = require("telescope.builtin")
telescope.load_extension "file_browser"

-- Override settings below if needed
telescope.setup {
  pickers = {
    lsp_references = { show_line = false }
  }
}

local function current_buffer_fuzz()
  -- Saner config than the default
  builtin.current_buffer_fuzzy_find({ sorting_strategy = "ascending" })
end

vim.keymap.set("n", "<c-_>", current_buffer_fuzz, { desc = "Fuzzy search current buffer" })

-- Hack for bug when opening in insert mode
-- Hack taken from: https://github.com/tmhedberg/SimpylFold/issues/130#issuecomment-1074049490
vim.api.nvim_create_autocmd("BufRead", {
  callback = function()
    vim.api.nvim_create_autocmd("BufWinEnter", {
      once = true,
      command = "normal! zx",
    })
  end
})
