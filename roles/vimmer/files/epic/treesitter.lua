-- nvim-treesitter config
require "nvim-treesitter.configs".setup {
  -- Installs missing parsers when entering a buffer
  auto_install = true,
  -- Install all the parsers, even if we don't use them all
  ensure_installed = "all",
  -- Don't use the slower built-in regex parser
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
  indent = {
    enable = true,
    disable = {
      "python",
    },
  },
  -- Playground to see colors
  -- requires the playground plugin to be installed
  playground = {
    enable = true,
  }
}

-- Enable syntax folding
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
vim.opt.foldenable = true
vim.opt.foldlevelstart = 99
-- Use <space><space> to toggle fold
vim.keymap.set("n", "<leader><leader>", "za", { desc = "Fold on line" })
-- Use <space>? to show the capture groups under the cursor
vim.keymap.set("n", "<leader>?", ":TSCaptureUnderCursor<cr>", { desc = "Show highlight groups under cursor" })
