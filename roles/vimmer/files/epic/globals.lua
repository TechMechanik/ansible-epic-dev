--- Global helper functions.

--- Wrapper to easily print or notify a value that neovim can inspect, such as a table.
---@param v any the value to inspect.
---@param opts table | nil extra options to modify behavior.
---@return nil
function P(v, opts)
  if opts and opts.notify then
    vim.notify(vim.inspect(v))
  else
    print(vim.inspect(v))
  end
end
