-- DAP (Debug Adapter Protocol) config

-- DAP itself
local dap = require("dap")
-- Extra UI plugin
local ui = require("dapui")
-- Virtual text plugin
require("nvim-dap-virtual-text").setup()

-- Adapters
local python = require("dap-python")

ui.setup()
python.setup(nil)

-- Base DAP config
dap.configurations.lua = {
  {
    type = "nlua",
    request = "attach",
    name = "Attach to running Neovim instance",
  }
}

dap.adapters.nlua = function(callback, config)
  callback({ type = "server", host = config.host or "127.0.0.1", port = config.port or 8086 })
end

-- Key mappings
vim.keymap.set("n", "<F5>", dap.continue, { desc = "DAP Start/Continue debugging" })
vim.keymap.set("n", "<F8>", dap.step_over, { desc = "DAP Step Over" })
vim.keymap.set("n", "<F9>", dap.step_into, { desc = "DAP Step Into" })
vim.keymap.set("n", "<F10>", dap.step_out, { desc = "DAP Step Out" })

vim.fn.sign_define("DapBreakpoint", { text = "🅱️" })

-- Open/close the UI automatically
dap.listeners.after.event_initialized["epic_nvim"] = function()
  ui.open()
end
-- TODO: handle closing once everything works properly; as is, it closes on errors and hides details
--[[
dap.listeners.after.event_exited["epic_nvim"] = function()
  ui.close()
end
dap.listeners.after.event_terminated["epic_nvim"] = function()
  ui.close()
end
]]

-- Exportable functions
local M = {
  dap_launch_from_json = function()
    -- Default path is .vscode/launch.json, but keep it in the project root
    require("dap.ext.vscode").load_launchjs("launch.json")
    dap.continue()
  end
}

return M
