-- LuaSnip snippets for html files
-- Disable modelines because it gets confused by the ftdj trigger
-- vi: nomodeline

local ls = require "luasnip"
local s = ls.snippet
local fmt = require("luasnip.extras.fmt").fmt

return {
  -- regular snippets
}, {
  -- Autosnippets
  s(
  -- Set file type to htmldjango
    { trig = "ftdj!" },
    fmt([[
        {{% comment %}}
          vi: ft=htmldjango
        {{% endcomment %}}
      ]], {})
  )
}
