--- LuaSnip snippets for Lua.

local ls = require("luasnip")
local s = ls.snippet
local i = ls.insert_node
local fmt = require("luasnip.extras.fmt").fmt

return {
  -- Regular snippets
  s(
  -- Test file describe block
    { trig = "describe" },
    fmt([[
    describe("{}", function()
      {}
    end)
  ]], { i(1, "Test description"), i(0) })
  ),
  s(
  -- Test block
    { trig = "it()" },
    fmt([[
      it("{}", function()
        {}
      end)
    ]], { i(1, "Can do something"), i(0) })
  ),
}
