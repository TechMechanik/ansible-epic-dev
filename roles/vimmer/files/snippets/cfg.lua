-- LuaSnip snippets for config files

local ls = require "luasnip"
local s = ls.snippet
local t = ls.text_node

return {}, {
  -- autosnippets
  s(
    { trig = "ansible.cfg" },
    t({
      "[defaults]", "",
      "# Add your targets in a file named inventory", "",
      "inventory = inventory", "",
      "# set a longer connection timeout; default is 10 seconds", "",
      "timeout = 60",
    })
  )
}
