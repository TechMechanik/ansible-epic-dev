-- LuaSnip snippets for python
local ls = require "luasnip"
local s = ls.snippet
local i = ls.insert_node
local t = ls.text_node
local fmt = require("luasnip.extras.fmt").fmt

return {
  -- Regular snippets
  s(
  -- Shebang
    { trig = "#!" },
    fmt([[
      #!/usr/bin/env python
      """{}"""

      {}
    ]], { i(1, "Docstring"), i(0) })
  ),
  s(
    { trig = "def" },
    fmt([[
      def {}({}) -> {}:
          """{}"""

          {}
    ]], { i(1, "func"), i(2, "*args, **kwargs"), i(3, "None"), i(4, "Docstring"), i(0, "pass") })
  )
}, {
  -- Autosnippets
  s(
  -- Django Debug mode
    { trig = "GETDEBUG" },
    t([[DEBUG = os.environ.get("DJANGO_DEBUG") == "TRUE"]])
  ),
  s(
  -- Django Secret Key
    { trig = "GETSECRET" },
    t([[SECRET_KEY = os.environ.get("DJANGO_SECRET_KEY")]])
  ),
  s(
  -- Main guard
    { trig = "ifmain" },
    t({ [[if __name__ == "__main__":]], "    # Main guard", "    main()" })
  ),
}
