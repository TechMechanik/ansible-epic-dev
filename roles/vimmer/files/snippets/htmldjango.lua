--- LuaSnip snippets for Django templates

local ls = require "luasnip"
local s = ls.snippet
local i = ls.insert_node
local fmt = require("luasnip.extras.fmt").fmt

return {
  -- regular snippets
  s(
    { trig = "if" },
    fmt([[
      {{% if {} %}}
        {}
      {{% endif %}}
      {}
    ]], { i(1, "condition"), i(2, "..."), i(0) })
  ),
  s(
    { trig = "for" },
    fmt([[
      {{% for {} %}}
        {}
      {{% endfor %}}
      {}
    ]], { i(1, "counter"), i(2, "..."), i(0) })
  ),
  s(
    { trig = "comment" },
    fmt([[
      {{% comment %}}
      {}
      {{% endcomment %}}{}
    ]], { i(1, "This is a comment"), i(0) })
  ),
}
