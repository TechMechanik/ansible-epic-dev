--- LuaSnip snippets for toml

local ls = require("luasnip")
local s = ls.snippet
local i = ls.insert_node
local fmt = require("luasnip.extras.fmt").fmt

return {
  -- Regular snippets
  s(
  -- Black config
    { trig = "[tool.black]" },
    fmt([[
      [tool.black]
      line-length = {}
      target-version = ['{}']
      {}
    ]], { i(1, "119"), i(2, "py311"), i(0) })
  ),
  s(
  --MyPy config
    { trig = "[tool.mypy]" },
    fmt([=[
      [tool.django-stubs]
      # Change this to your django settings module
      django_settings_module = "settings"

      [tool.mypy]
      exclude = [
        "migrations",
        "tests",
        "manage.py",
        "(a|w)sgi.py",
      ]
      plugins = [
        "mypy_django_plugin.main",
      ]

      [[tool.mypy.overrides]]
      module = [
        "djangorestframework.*",
        "wagtail.*",
      ]
      ignore_missing_imports = true
    ]=], {})
  ),
  s(
  -- Pytest config
    { trig = "[tool.pytest.ini_options]" },
    fmt([[
      [tool.pytest.ini_options]
      minversion = "{}"
      addopts = "{}"
      {}
    ]], { i(1, "6.0"), i(2, "-v"), i(0) })
  ),
  s(
  -- Ruff config
    { trig = "[tool.ruff]" },
    fmt([[
      [tool.ruff]
      extend-exclude = ["migrations"]
      line-length = 119
      target-version = "py310"

      [tool.ruff.lint]
      extend-select = ["A", "B", "C4", "C90", "COM", "D", "DJ", "I", "LOG", "N", "PTH", "RUF", "SIM", "W"]
      ignore = ["D203", "D213"]
    ]], {})
  ),
}
