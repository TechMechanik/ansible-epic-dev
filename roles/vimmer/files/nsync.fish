function nsync --description "Start nvim with only the plugins.lua file imported and run :PackerSync"
  nvim -u ~/.config/nvim/packersync.lua
end
