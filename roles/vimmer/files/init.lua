-- Vim configuration file

-- Define some globals before importing anything else.
require("epic.globals")

-- Bootstrap lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Override filetypes
vim.filetype.add({
  extension = {
    html = "htmldjango",
  }
})

-- Set the leader to space
vim.g.mapleader = " "

-- Disable the mouse entirely
vim.opt.mouse = {}

-- Disable the providers we won't use
vim.g.loaded_node_provider = 0
vim.g.loaded_perl_provider = 0
vim.g.loaded_ruby_provider = 0

-- Display
vim.opt.cursorline = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.termguicolors = true
vim.cmd([[colorscheme epic]])
-- Sign column always visible, as it's annoying otherwise with lspsaga lightbulbs
vim.opt.signcolumn = "yes"
-- Information about the current file
vim.opt.winbar = [[%=%m %y %f:%l:%c]]
-- Show invisible characters
vim.opt.list = true
vim.opt.listchars = {
  tab = [[▸\]],
  trail = "·",
}

-- Timeout after which WhichKey will display completions
vim.opt.timeout = true
vim.opt.timeoutlen = 300

-- Formatting
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2
vim.opt.expandtab = true
-- Scrolling offset - start scrolling before reaching the bottom/top
vim.opt.scrolloff = 10
vim.opt.sidescrolloff = 10

-- Opening splits should open them after, not before
vim.opt.splitright = true
vim.opt.splitbelow = true

-- Plugin configs
-- Plugins managed by lazy.nvim
require("epic.plugins")
-- Simple plugin config that doesn't deserve its own files
require "nvim-web-devicons".setup {}
-- ISO 8601, motherfuckers
vim.g.gitblame_date_format = [[%Y-%m-%d %X (%r)]]
-- Debug Adapter Protocol
require("epic.dap")
-- Enable notify
require("epic.notify")
-- Lualine status line
require("epic.lualine")
-- Configure LSP
require("epic.lsp")
-- Tree-sitter
require("epic.treesitter")
-- Git integration
require("epic.gitsigns")
-- Telescope config
require("epic.telescope")
-- Autocompletion
require("epic.cmp")
-- Enable LuaSnip
require("epic.luasnip")
-- Better tmux integration
require("epic.nvim_tmux")
-- Testing
require("epic.neotest")
-- Key completion
require("epic.which_key")

-- Import local overrides, if defined. If the file doesn't exist, ignore amy errors.
pcall(require, "epic.local_config")

-- Global key mappings
vim.keymap.set("n", "<Esc>", ":nohl<cr>", { desc = "Disable highlighting" })
-- Center after movement
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "n", "nzz")
vim.keymap.set("n", "N", "Nzz")
vim.keymap.set("n", "{", "{zz")
vim.keymap.set("n", "}", "}zz")

-- Quit window with <C-c>
vim.keymap.set("n", "<C-c>", "<cmd>q<cr>")
